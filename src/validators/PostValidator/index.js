import { body, param} from 'express-validator'

class PostValidator {
  static create = () => [
    body('title').exists().isString(),
    body('body').exists().isString(),
    body('authorId').exists().isUUID(),
  ]
  static update = () => [
    param('id').exists().isUUID(),
    body('title').exists().isString(),
    body('body').exists().isString(),
    body('authorId').exists().isUUID(),
  ]
  static delete = () => [
    param('id').exists().isUUID(),
  ]
}

export default PostValidator
