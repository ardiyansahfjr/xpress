import { Post } from '../../models'

class PostController {
  static get = (req, res) => Post.findAll().then(
    (data) => res.status(200).json(data),
  ).catch(
    (e) => console.log(e),
  )

  static create = (req, res) => {
    const { title, body, authorId } = req.body

    return Post.create({
      title,
      body,
      authorId
    }).then(
      (data) => res.status(201).json({ ...data.dataValues }),
    ).catch(
      (e) => console.log(e),
    )
  }

  static update = (req, res) => {
    const { id } = req.params

    return Post.findOne({
      where: { id },
    }).then(
      (post) => {
        if (!post) return res.status(404).json({ message: 'Not found' })

        const { title, body, authorId } = req.body

        return post.update(
          { title, body, authorId },
          { returning: true },
        ).then(
          (updated) => res.status(200).json({ ...updated.dataValues }),
        )
      },
    ).catch(
      (e) => console.log(e),
    )
  }

  static delete = (req, res) => {
    const { id } = req.params

    return Post.destroy({
      where: { id },
    }).then(
      (data) => {
        if (!data) return res.status(404).json({ message: 'Not found' })

        return res.status(200).json({ message: 'Deleted' })
      },
    ).catch(
      (e) => console.log(e),
    )
  }
}

export default PostController
